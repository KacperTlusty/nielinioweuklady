step = 0.01;
scale = 1000;
t = 0:step:2*pi;
arr_a = sin(t).*scale;
arr_b = sin(t + 2./3.*pi).*scale;
arr_c = sin(t + 4./3.*pi).*scale;

for i = 2:length(arr_a)
    val_a = num2str(round(arr_a(i) - arr_a(i - 1)));
    val_b = num2str(round(arr_b(i) - arr_b(i - 1)));
    val_c = num2str(round(arr_c(i) - arr_c(i - 1)));

    dest = ['a ', val_a, ' b ', val_b, ' c ', val_c];
     
    fprintf(s, dest);
    while(~s.BytesAvailable)
    end
    while(s.BytesAvailable)
        fscanf(s);
    end
end