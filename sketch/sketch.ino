#include <stdlib.h>

#define X_STEP_PIN         54
#define X_DIR_PIN          55
#define X_ENABLE_PIN       38
#define Y_STEP_PIN         60
#define Y_DIR_PIN          61
#define Y_ENABLE_PIN       56
#define Z_STEP_PIN         46
#define Z_DIR_PIN          48
#define Z_ENABLE_PIN       62
#define X_MAX_PIN          2
#define Y_MAX_PIN          15
#define Z_MAX_PIN          19
#define DELAY_TIME         1

bool dir_x, dir_y, dir_z, flag = false;
int engineStep;
String a;
int step_x = 0, step_y = 0, step_z = 0;

void setup() {
  Serial.begin(115200);
  pinMode(X_ENABLE_PIN, OUTPUT);
  digitalWrite(X_ENABLE_PIN, LOW);
  pinMode(X_DIR_PIN, OUTPUT);
  digitalWrite(X_DIR_PIN, HIGH);
  pinMode(X_STEP_PIN, OUTPUT);
  pinMode(Y_ENABLE_PIN, OUTPUT);
  digitalWrite(Y_ENABLE_PIN, LOW);
  pinMode(Y_DIR_PIN, OUTPUT);
  digitalWrite(Y_DIR_PIN, HIGH);
  pinMode(Y_STEP_PIN, OUTPUT);
  pinMode(Z_ENABLE_PIN, OUTPUT);
  digitalWrite(Z_ENABLE_PIN, LOW);
  pinMode(Z_DIR_PIN, OUTPUT);
  digitalWrite(Z_DIR_PIN, HIGH);
  pinMode(Z_STEP_PIN, OUTPUT);
  pinMode(X_MAX_PIN, INPUT_PULLUP);
  pinMode(Y_MAX_PIN, INPUT_PULLUP);
  pinMode(Z_MAX_PIN, INPUT_PULLUP);
  dir_x = false;
  dir_y = false;
  dir_z = false;
}

void loop() {  
  if ((!digitalRead(X_MAX_PIN) || dir_x) && step_x > 0) {
    digitalWrite(X_STEP_PIN, HIGH);
    step_x--;
  }
  if ((!digitalRead(Y_MAX_PIN) || dir_y) && step_y > 0) {
    digitalWrite(Y_STEP_PIN, HIGH);
    step_y--;
  }
  if ((!digitalRead(Z_MAX_PIN) || dir_z) && step_z > 0) {
    digitalWrite(Z_STEP_PIN, HIGH);
    step_z--;
  }
  delay(DELAY_TIME);

  digitalWrite(X_STEP_PIN, LOW);
  digitalWrite(Y_STEP_PIN, LOW);
  digitalWrite(Z_STEP_PIN, LOW);
  while(Serial.available() > 0) {
     a = Serial.readStringUntil('\n');
     sscanf(a.c_str(), "a %d b %d c%d", &step_x, &step_y, &step_z);
     dir_x = step_x < 0;
     dir_y = step_y < 0;
     dir_z = step_z < 0;
     flag = true;
     digitalWrite(X_DIR_PIN, dir_x);
     digitalWrite(Y_DIR_PIN, dir_y);
     digitalWrite(Z_DIR_PIN, dir_z);
     step_x = abs(step_x);
     step_y = abs(step_y);
     step_z = abs(step_z);
  }

  if (step_x == 0 && step_y == 0 && step_z == 0 && flag) {
    flag = false;
    Serial.println("done");
  }
  
  delay(DELAY_TIME);
}
