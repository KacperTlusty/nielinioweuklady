#include <LiquidCrystal.h>
#include <math.h>

LiquidCrystal lcd(16, 17, 23, 25, 27, 29);

#define HEATER_0 10

#define TEMP_0_PIN         13   // Analog Input
#define TEMP_1_PIN         15   // Analog Input
#define TEMP_BED_PIN       14   // Analog Input

#define a -4.672495427933098e-07
#define b 7.856982112902327e-04
#define c -0.541931993388775
#define d 2.390500518497808e+02
#define TEMP 100
#define K 1 
int x = 0;

double temperature;
double t;

void setup() {
  lcd.begin(20, 4);
  // put your setup code here, to run once:
  pinMode(TEMP_0_PIN, INPUT);
  pinMode(TEMP_1_PIN, INPUT);
  pinMode(TEMP_BED_PIN, INPUT);
  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:
  lcd.setCursor(0, 0);
  t = (double)analogRead(TEMP_0_PIN);
  temperature = a * pow(t, 3.0) + b * pow(t, 2.0) + c * t + d;
  lcd.print(temperature);
  analogWrite(HEATER_0, round(3.1875 * (TEMP - temperature)));
}
